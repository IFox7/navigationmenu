import React from "react";
// Імпортуємо елементи навігаційного меню
import ElementMenu from "../../elements/element_menu";
import HeaderMenu from "../header_menu";
import FooterMenu from "../footer_menu";
// Імпортуємо іконки
import search from "../element_menu/icons_img/search.svg";
import dashboard from "../element_menu/icons_img/dashboard.svg";
import revenue from "../element_menu/icons_img/revenue.svg";
import notification from "../element_menu/icons_img/notification.svg";
import analytics from "../element_menu/icons_img/analytics.svg";
import inventory from "../element_menu/icons_img/inventory.svg";

import logout from "../footer_menu/footer_img/logout.svg";
// Імпортуємо стилі
import "./style.css";

function App() {
  return (
    <>
      {/* Блок звуженого навігаційного меню */}
      <div className="pannel_wrapper_narrow">
        <div className="header_badge_narrow">AF</div>
        <div className="body_wrapper_narrow">
          <ElementMenu img={search} className="element_narrow" />
          <ElementMenu img={dashboard} className="element_narrow" />
          <ElementMenu img={revenue} className="element_narrow" />
          <ElementMenu img={notification} className="element_narrow" />
          <ElementMenu img={analytics} className="element_narrow" />
          <ElementMenu img={inventory} className="element_narrow" />
        </div>
        <div className="footer_narrow">
          <img className="img_footer" src={logout} alt="" />
          <div className="moon_sun_buttonwrapper">
            <input type="checkbox" className="button_footer"></input>
          </div>
        </div>
      </div>
      {/* Блок розширеного меню, що виїзджає при наведенні курсора */}
      <div id="panel">
        <div id="hidden_panel">
          <div className="pannel_wrapper">
            <HeaderMenu />
            <div className="body_wrapper">
              <ElementMenu img={search} title="Search..." />
              <ElementMenu img={dashboard} title="Dashboard" />
              <ElementMenu img={revenue} title="Revenue" />
              <ElementMenu img={notification} title="Notification" />
              <ElementMenu img={analytics} title="Analytics" />
              <ElementMenu img={inventory} title="Inventory" />
            </div>
            <FooterMenu />
          </div>
        </div>
      </div>
    </>
  );
}
export default App;
