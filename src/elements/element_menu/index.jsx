import React from "react";
// Імпорт стилів
import "./style.css";
// Основний елемент навігаційого меню(середня частина)
function ElementMenu(props) {
  return (
    <>
      <div className="element_menu">
        <div>
          <img src={props.img} alt="icon_img" />
        </div>
        <div className="body_elem_title">{props.title}</div>
      </div>
    </>
  );
}

export default ElementMenu;
