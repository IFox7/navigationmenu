import React from 'react'
// Імпорт іконок футера
import moon from "./footer_img/moon_img.svg"
import logout from"./footer_img/logout.svg"
// Імпорт стилів
import "./style.css"
// Елемент футера навігаційного меню
function FooterMenu() {
    return (
    <>
    <div className="footer_wrapper">
        <div className="logout">
            <img src={logout} alt="" />
              Logout
            </div>
        <div className="darkmode_wrapper">
            <div className="darkmode">
            <img src={moon} alt="" />
                  Darkmode
            </div>
            <div className="moon_sun_buttonwrapper">
             <input type="checkbox"className='button_footer'></input>
            </div>
        </div>
    </div>
    </>
  )
}

export default FooterMenu