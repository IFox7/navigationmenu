import React from "react";
// Імпорт стилів
import "./style.css";
// Елемент хедера меню
function HeaderMenu() {
  return (
    <>
      <div className="header_wrapper">
        <div className="header_badge">AF</div>
        <div className="header_name">
          <div className="header_title">AnimatedFred</div>
          <div className="header_mail">animated@demo.com</div>
        </div>
      </div>
    </>
  );
}

export default HeaderMenu;
