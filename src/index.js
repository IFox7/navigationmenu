import React from "react";
import ReactDOM from "react-dom/client";
// Імпорт всього навігаційного меню
import App from "./elements/APP/App";
// Розміщення навігаційного меню на HTML сторінці
const root = ReactDOM.createRoot(document.getElementById("root"));
root.render(<App />);
